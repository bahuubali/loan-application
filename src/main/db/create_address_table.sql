CREATE TABLE `address` (
	  `address_id` int(11) NOT NULL,
	  `customer_id` int(11) NOT NULL,
	  `address_1` varchar(50) NOT NULL,
	  `address_2` varchar(50) DEFAULT NULL,
	  `address_3` varchar(45) DEFAULT NULL,
	  PRIMARY KEY (`address_id`),
	  KEY `KF_customer_id_idx` (`customer_id`),
	  CONSTRAINT `KF_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
