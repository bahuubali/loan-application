CREATE TABLE `loan_request_details` (
  `loan_request_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `requested_amount` int(11) NOT NULL,
  `monthly_salary` int(11) NOT NULL COMMENT '1 if the person is eligible for loan. 0 otherwise.',
  `eligible` int(11) DEFAULT NULL,
  PRIMARY KEY (`loan_request_id`),
  KEY `FK_customer_id_idx` (`customer_id`),
  CONSTRAINT `FK_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Loan request details.';
