package com.loanapp.exceptions;

public class CustomerNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8389109538349957485L;
	private String errorMessage;
	
	public CustomerNotFoundException() {
		super();
	}

	public CustomerNotFoundException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
}
