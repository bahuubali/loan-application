package com.loanapp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.loanapp.dao.CustomerDAO;
import com.loanapp.model.Customer;

@Component
public class CustomerDAOImpl implements CustomerDAO{

	@Autowired
    JdbcTemplate jdbcTemplate;

	public int addCustomer(Customer customer) {
		jdbcTemplate.update("INSERT INTO customer ( first_name, last_name, dob,pan,email_id) VALUES (?, ?, ?, ?,?)",
		           customer.getFirstName(), customer.getLastName(), new java.sql.Date(2017,11,18),customer.getPan(),customer.getEmail());
		        System.out.println("Customer Added!!");
			return 0;		
	}
}
