package com.loanapp.dao;

import java.util.List;

import com.loanapp.model.Customer;

public interface CustomerDAO {

	int addCustomer(Customer customer);
}
