package com.loanapp.service;

import java.util.List;

import com.loanapp.exceptions.CustomerDeleteException;
import com.loanapp.exceptions.CustomerNotFoundException;
import com.loanapp.model.Customer;

public interface CustomerService {

	Customer create(Customer customer);
}
