package com.loanapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.loanapp.dao.CustomerDAO;
import com.loanapp.exceptions.CustomerDeleteException;
import com.loanapp.exceptions.CustomerNotFoundException;
import com.loanapp.model.Customer;
import com.loanapp.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDAO customerDAO;

	public Customer create(Customer customer) {
		System.out.println("Service()");
		customerDAO.addCustomer(customer);
		return customer;
	}

}
